rm(list = ls())

library(STRINGdb)
library(MCL)
library(reshape2)
library(igraph)
library(dplyr)
library(ggplot2)
library(corpcor)

load("LAM VSN by uniprot.RData")
load("String db interactions.RData")
pep2str = read.table(file = "86312 Array peptides string enriched.txt", header = TRUE, sep = "\t")

pval = read.table("LAM TTP peptides 3 .txt", header = TRUE, sep= "\t")
lam = aCube
mapfun = function(df){
  idx = grep(df$UniprotAccession, pep2str$Uniprot)
  df$STRING_id = pep2str$STRING_id[idx[1]]
  return(df)
}

mapped_lam = lam %>% group_by(UniprotAccession) %>% do(mapfun(.))
mapped_lam = subset(mapped_lam, !is.na(STRING_id))

X = acast(mapped_lam, Sample.name ~ STRING_id)
R = cor(X)
pcor = cor2pcor(R)# may want to use partial cor without ppi

int.df = subset(int.df, combined_score >400)
df_in = int.df[c("from", "to", "combined_score")]
gr = graph_from_data_frame(df_in, directed = FALSE)
adj = as.matrix(as_adjacency_matrix(gr))
ovrlap = intersect(colnames(X), colnames(adj))
bX = colnames(R) %in% ovrlap
bA = colnames(adj)%in%ovrlap
R = R[bX, bX]
pcor = pcor[bX, bX]
adj = adj[bA,bA]
R = R[order(colnames(R)), order(colnames(R))]
#R[R<0] = 0
adj = adj[order(colnames(adj)), order(colnames(adj))]
#cl = mcl(adj * R^2,inflation = 2.2, expansion = 2 , addLoops = TRUE, allow1 = TRUE)

#R[abs(R)< 0.5] = 0
A = adj*R^2
#A = abs(pcor)
# A[abs(A)< 0.25] = 0
diag(A) = 0
clGr = graph_from_adjacency_matrix(A, mode = "undirected", weighted = TRUE)

com = cluster_walktrap(clGr, steps = 10)
w = ifelse(crossing(com, clGr), 1,0.2)
cllo = layout_with_kk(clGr, weights = w)

prt = ggplot(pval, aes(x = as.character(unicol), y = X1_LAM..TTest_p,fill = X1_LAM..TTest_delta))
prt = prt + scale_fill_gradient2(low = "blue", high = "orange")
bf = ggplot_build(prt)
cdf = as.data.frame(bf$data)
pval$fill = cdf$fill
pval$fill[pval$X1_LAM..TTest_p > 0.05] = "#FFFFFF"

name = vector()
colour = vector()
for(i in 1:dim(adj)[1]){
  name[i] = as.character(mapped_lam$UniprotAccession[grep(rownames(adj)[i], mapped_lam$STRING_id)])
  colour[i] = as.character(pval$fill[grep(name[i], pval$unicol)])
}

vertex_attr(clGr) = list(color = com$membership, name = com$membership)
plot(clGr, layout = cllo)

vertex_attr(clGr) = list(color = colour, name = com$membership)
plot(clGr, layout = cllo)

vertex_attr(clGr) = list(color = colour, name = name)

for (i in 1:max(com$membership)){
  idx = (1:dim(adj)[1])[i == com$membership]
  if(length(idx) > 1){
  aClust = subgraph(clGr, idx)
  plot(aClust, main = paste("com", i))
  }
}
  


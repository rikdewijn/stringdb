rm(list = ls())

library(STRINGdb)
library(MCL)
library(reshape2)
library(igraph)
library(dplyr)
library(ggplot2)
library(corpcor)

load( "Dub_log_data.RData")
load("String db interactions.RData")
pep2str = read.table(file = "86312 Array peptides string enriched.txt", header = TRUE, sep = "\t")
pval = read.table("Dub_GeneGo.txt", header = TRUE, sep= "\t")

df = aCube
mapfun = function(df){
  idx = grep(df$UniprotAccession, pep2str$Uniprot)
  df$STRING_id = pep2str$STRING_id[idx[1]]
  return(df)
}

mapped_df = df %>% group_by(UniprotAccession) %>% do(mapfun(.))
mapped_df = subset(mapped_df, !is.na(STRING_id))

# create adj matrix weighted by correlations
X = acast(mapped_df, colSeq ~ STRING_id, fun.aggregate = mean)
R = cor(X)
int.df = subset(int.df, combined_score >400)
df_in = int.df[c("from", "to", "combined_score")]
gr = graph_from_data_frame(df_in, directed = FALSE)
adj = as.matrix(as_adjacency_matrix(gr))

# match
ovrlap = intersect(colnames(X), colnames(adj))
bX = colnames(R) %in% ovrlap
bA = colnames(adj)%in%ovrlap
R = R[bX, bX]
adj = adj[bA,bA]
R = R[order(colnames(R)), order(colnames(R))]
adj = adj[order(colnames(adj)), order(colnames(adj))]
A = adj*R^2

#cluster
clGr = graph_from_adjacency_matrix(A, mode = "undirected", weighted = TRUE)
com = cluster_walktrap(clGr, steps = 10)

# clustered plot lay-out
w = ifelse(crossing(com, clGr), 1,0.5)
cllo = layout_with_kk(clGr, weights = w)

# colours by delta ()

pval$fill.col = pval$Treatment1_cmt..out___LogFC
pval$p.col = pval$Treatment1_cmt..out___pvalue.treatment
title = "Treatment 1"

prt = ggplot(pval, aes(x = as.character(unicol), y = p.col,
                       fill = fill.col))
prt = prt + scale_fill_gradient2(low = "blue", high = "red")
bf = ggplot_build(prt)
cdf = as.data.frame(bf$data)

pval$fill = cdf$fill
pval$fill[pval$p.col> 0.05] = "#FFFFFF"

name = vector()
colour = vector()
for(i in 1:dim(adj)[1]){
  matchIdx = grep(rownames(adj)[i], mapped_df$STRING_id)
  name[i] = as.character(mapped_df$ID[matchIdx])
  uni = as.character(mapped_df$UniprotAccession[matchIdx])
  colour[i] = as.character(pval$fill[grep(uni, pval$unicol)])
}

vertex_attr(clGr) = list(color = com$membership, name = com$membership)
plot(clGr, layout = cllo, main = "Clusters")

vertex_attr(clGr) = list(color = colour, name = com$membership)
plot(clGr, layout = cllo, main = title)

vertex_attr(clGr) = list(color = colour, name = name)
cIdx = 4
aClust = subgraph(clGr, (1:dim(adj)[1])[cIdx == com$membership])
plot(aClust, main = paste(title, "mapped on cluster", cIdx), vertex.label.cex = 0.7)

lookup = subset(mapped_df, colSeq ==1)
clNames = vertex.attributes(aClust)$name
clStringID = lookup$STRING_id[ lookup$ID%in%clNames]

urlBase = "http://string-db.org/newstring_cgi/show_network_section.pl?identifiers="
strid = paste(clStringID, collapse = "%0A")
par = paste("?limit=", length(clStringID), sep = "")
cluster.url = paste(urlBase, strid, par)
